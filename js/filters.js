$(document).ready(function(){
    filterInit();
});

function filterInit(){
    $('.filter').on('click', function(){
        let input = $(this).find('input');
        let filterId = input.attr('id');
        let imgLinked = $('#content img[data-id="' + filterId + '"]');
        if( input.prop('checked') )
        {
            imgLinked.show();
        }
        else
        {
            imgLinked.hide();
        }
    });
}

